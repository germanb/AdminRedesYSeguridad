#!/usr/bin/python

import sys
import binascii
import itertools
import random


mensajeOriginal = sys.argv[1]
longitud = len(mensajeOriginal)

mult = 1
i = 1
while i < longitud:
    mult = mult * 10
    i = i + 1

random.seed("clave")
cadenaCifradora = str(random.randint(mult, (mult*10)-1))

list1 = list(mensajeOriginal.encode('utf-8'))
list2 = list(cadenaCifradora.encode('utf-8'))
resultado = []
#xorg es ^
for a, b in zip(list1,list2):
    resultado.append(a ^ b)
cadena = bytes(resultado)
print(resultado)
print(cadena)
texto = binascii.hexlify(cadena)
print(texto)
