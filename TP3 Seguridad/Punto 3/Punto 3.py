#!/usr/bin/python

import sys
import binascii
import itertools

buffer1 = sys.argv[1]
caracter = sys.argv[2]
if len(caracter) != 1:
    print("El segundo argumento debe ser un unico caracter")
else:
    caracter = ord(caracter)
    buffer1 = binascii.unhexlify(buffer1)
    lista = list(buffer1)
    resultado = []
    #xorg es ^
    for a in lista:
        result = a ^ caracter
        resultado.append(result)
    cadena = bytes(resultado)
    texto = cadena.decode('utf-8')
    print(texto)
