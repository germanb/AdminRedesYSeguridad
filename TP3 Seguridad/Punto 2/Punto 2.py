#!/usr/bin/python

import sys
import binascii
import itertools

buffer1 = sys.argv[1]
buffer2 = sys.argv[2]
if len(buffer1) == len(buffer2):
    list1 = list(buffer1.encode('utf-8'))
    list2 = list(buffer2.encode('utf-8'))

    resultado = []

    #xorg es ^
    for a, b in zip(list1,list2):
        resultado.append(a ^ b)
    cadena = bytes(resultado)
    texto = binascii.hexlify(cadena)
    print(texto)
else:
    print("ERROR: Las cadenas no son de igual longitud.")
