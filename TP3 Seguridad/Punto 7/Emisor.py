#!/usr/bin/python

import sys
import socket
from pdb import set_trace

numeroComun = 557
modulo = 839
numeroSecreto = 78
mandar = numeroComun ** numeroSecreto % modulo
# RECIBE computar
HOST = 'localhost'
PORT = 8081
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, int(sys.argv[1])))
s.listen(1)
conn, addr = s.accept()
computar = int(conn.recv(1024).decode('utf-8'))
# MANDA mandar
conn.send(str(mandar).encode('utf-8'))
conn.close()
secreto = computar**numeroSecreto % modulo
print("El secreto compartido es: " + str(secreto))
