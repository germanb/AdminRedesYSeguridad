#!/usr/bin/python

import sys
import socket
from pdb import set_trace


numeroComun = 557
modulo = 839
numeroSecreto = 26
mandar = numeroComun ** numeroSecreto % modulo
# MANDA mandar
HOST = 'localhost'
PORT = 8081
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, int(sys.argv[1])))
s.send(str(mandar).encode('utf-8'))
# RECIBE computar
computar = s.recv(1024).decode('utf-8')
computar = int(computar)
s.close()
secreto = computar**numeroSecreto % modulo
print("El secreto compartido es: " + str(secreto))
