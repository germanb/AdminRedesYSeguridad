#!/usr/bin/python

import sys
import binascii
import itertools

cadenaCod = '''29135a0e1f5a0c1f155a1b
1715085a1e1f165a150e08
155a161b1e155a7014155a
0c15035a1b5a1e0f1e1b08
565a700e151e155a16155a
0b0f1f5a0c1f155a7017b9
db095a0e151e155a16155a
0b0f1f5a09131f140e1554
5a707029135a0e1f5a0c1f
155a1b1715085a1e1f165a
150e08155a161b1e155a70
03155a0c15035a1b5a1908
0f001b08565a700e151e15
5a16155a0b0f1f5a0e1f14
1d155a701f095a0e151e15
5a16155a0b0f1f5a13140e
1f140e15545a7070702f14
5a191b08141b0c1b16565a
700f145a08b9d7155a1808
1b0c15565a700f141b5a19
1b16161f5a1f145a191514
0e081b171b1415565a700f
145a1215090a130e1b165a
1b181b141e15141b1e1556
5a70161b5a1509190f0813
1e1b1e565a701e1f5a1915
081b00b9c9145a03155a0c
15035a1b141e1b141e155a
1e1f5a0e0f5a171b141554
5a70707029135a0e1f5a0c
1f155a1b1715085a1e1f16
5a150e08155a161b1e155a
7014155a0c15035a1b5a1e
0f1e1b08565a700e151e15
5a16155a0b0f1f5a0c1f15
5a7017b9db095a0e151e15
5a16155a0b0f1f5a09131f
140e15545a70707029135a
0e1f5a0c1f155a1b171508
5a1e1f165a150e08155a16
1b1e155a7003155a0c1503
5a1b5a19080f001b08565a
700e151e155a16155a0b0f
1f5a0e1f141d155a701f09
5a0e151e155a16155a0b0f
1f5a13140e1f140e15545a
7070702f145a0e1f170a15
081b16565a700f145a1913
0819155a171b1615565a70
0f141b5a0a161b031b5a09
13145a0c1f081b1415565a
700f145a1f090a13081b16
5a1b181b141e15141b1e15
565a70161b5a19161b0813
1e1b1e565a701e1f5a1915
081b00b9c9145a03155a0c
15035a1b141e1b141e155a
1e1f5a0e0f5a171b141554
'''

caracter = 0
cadenaCod = cadenaCod.replace("\n","")
cadenaCod = binascii.unhexlify(cadenaCod)
#cadenaCod = list(cadenaCod.encode('utf-8'))
masEspacios = 0
textoPosible = ""
while caracter < 256:
    caracter = caracter + 1
    resultado = []
    for a in cadenaCod:
        result = a ^ caracter
        resultado.append(result)
    try:
        cadena = bytes(resultado)
        texto = cadena.decode('utf-8')
        print(texto, file=open("decodificado.txt", "a"))
        espaciosTotales = 0
        for c in texto:
            if c == ' ':
                espaciosTotales = espaciosTotales + 1
        if espaciosTotales > masEspacios:
            masEspacios = espaciosTotales
            textoPosible = texto
        #print(texto)
    except Exception as e:
        pass
        #print("\nHubo error\n", file=open("decodificado.txt", "a"))
        #print("\nHubo error\n")
print(textoPosible)
