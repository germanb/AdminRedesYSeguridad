#!/usr/bin/python

import sys
import binascii
import itertools

buffer1 = sys.argv[1]
clave = "clave"
max = len(clave)

list1 = list(buffer1.encode('utf-8'))
list2 = list(clave.encode('utf-8'))

resultado = []

i = 0
#xorg es ^
for a in list1:
    resultado.append(a ^ list2[i])
    if i == max-1:
        i = 0
    else:
        i = i+1
cadena = bytes(resultado)
texto = binascii.hexlify(cadena)
print(texto)
